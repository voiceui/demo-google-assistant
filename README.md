# demo-google
Google Assistant demo


## References

* [Google Assistant SDK](https://developers.google.com/assistant/sdk/)
* [Google Assistant SDK Documentation](https://developers.google.com/assistant/sdk/overview)
* [Actions on Google](https://developers.google.com/actions/)
* [Actions on Google Developer Challenge](https://developers.google.com/actions/challenge/)

